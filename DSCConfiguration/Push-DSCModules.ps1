Configuration Push-DSCModules {
    param ()

    Import-DscResource -ModuleName 'PSDesiredStateConfiguration'

    Node localhost {
    
        File DSCResourceFolder {
            SourcePath = "$PSScriptRoot\..\DSCRessource"
            DestinationPath = "C:\Program Files\WindowsPowerShell\Modules\"
            Recurse = $true
            Type = "Directory"
        }
    }
}

Push-DSCModules -OutputPath $PSScriptRoot