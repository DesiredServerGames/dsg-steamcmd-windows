Configuration Push-SteamCMD
{
param()


Import-DscResource -ModuleName 'PSDesiredStateConfiguration', 'FileDownloadDSC'

    Node localhost
    {
         File SteamCMD {
            Type = 'Directory'
            DestinationPath = 'C:\steamcmd'
            Ensure = "Present"
         }
         FileDownload SteamCMD {
            FileName = "c:\steamcmd\steamcmd.zip"
            Url = "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip"
            DependsOn = "[File]SteamCMD"
        }
        Archive livraison {
			Ensure = "Present"
			Path = "c:\steamcmd\steamcmd.zip"
			Destination = "c:\steamcmd\"
			Force = $true
            DependsOn = "[FileDownload]SteamCMD"
		}
        WindowsProcess SteamCMD {
            path = "c:\steamcmd\steamcmd.exe"
            Arguments = "+login anonymous"
            WorkingDirectory = "c:\steamcmd\"
            Ensure = "present"
            DependsOn = "[Archive]livraison"
        }
    }
}

Push-SteamCMD -OutputPath $PSScriptRoot